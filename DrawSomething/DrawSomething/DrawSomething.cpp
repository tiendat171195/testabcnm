﻿// DrawSomething.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "DrawSomething.h"
#include <sstream>
//#include <AtlBase.h>
//#include <atlconv.h>
#include <commdlg.h>
#include <shellapi.h>
#define MAX_LOADSTRING 100
#define NUM_ALPHABET 26
// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
struct Something{
	TCHAR myChar[2];
	TCHAR myLink[MAX_LOADSTRING];
};

// Forward declarations of functions included in this code module:
//ATOM				MyRegisterClass(HINSTANCE hInstance);
//BOOL				InitInstance(HINSTANCE, int);
//LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
//INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	DialogProc(HWND, UINT, WPARAM, LPARAM);
void InitNotifyIconData(NOTIFYICONDATA &notifyIconData, HWND hWnd);
int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;
	
	DialogBox(hInstance, MAKEINTRESOURCE(IDD_MYDIALOG), 0, DialogProc);

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_DRAWSOMETHING));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

INT_PTR CALLBACK	DialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	static Something sthTable[NUM_ALPHABET];
	static OPENFILENAME ofn; // CTDL dùng cho dialog open
	static TCHAR szFile[256];
	static TCHAR szFilter[] = TEXT("Word file(*.doc)\0 * .doc\0Excel file(*.xls)\0 * .xls\0Text file(*.txt)\0 * .txt\0Thực Thi (*.exe)\0*.exe\0");
	szFile[0] = '\0';
	static TCHAR tempChar[2];
	static TCHAR tempLink[MAX_LOADSTRING];
	static NOTIFYICONDATA notifyIconData;

	switch (message)
	{
	case WM_INITDIALOG:
		for (int i = 0; i < NUM_ALPHABET; ++i){
			sthTable[i].myChar[0] = i + 65;
			sthTable[i].myChar[1] = '\0';
			sthTable[i].myLink[0] = '\0';
			SendDlgItemMessage(hDlg, IDC_COMBOCHAR, CB_ADDSTRING, (WPARAM)0, (LPARAM)sthTable[i].myChar);
		}
			
			
		// Khởi tạo struct
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hDlg; // handle của window cha
		ofn.lpstrFilter = szFilter;
		ofn.nFilterIndex = 1;
		ofn.lpstrFile = szFile; // chuỗi tên file trả về
		ofn.nMaxFile = sizeof(szFile);
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

		InitNotifyIconData(notifyIconData, hDlg);
		ShowWindow(hDlg, SW_SHOW);
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam)){
		case IDC_HIDE:
			ShowWindow(hDlg, SW_HIDE);
			Shell_NotifyIcon(NIM_ADD, &notifyIconData);
			break;
		case IDC_QUIT:
			EndDialog(hDlg, FALSE);
			break;
		case IDC_GET:
			GetWindowText(GetDlgItem(hDlg, IDC_COMBOCHAR), tempChar, 2);
			GetWindowText(GetDlgItem(hDlg, IDC_EDITLINK), tempLink, MAX_LOADSTRING);
			for (int i = 0; i < NUM_ALPHABET; i++)
			{
				if (wcscmp(tempChar, sthTable[i].myChar) == 0)
				{
					wcscpy_s(sthTable[i].myLink, tempLink);
				}
			}
			break;
		case IDC_BROWSE:
			GetOpenFileName(&ofn);
			SetWindowText(GetDlgItem(hDlg, IDC_EDITLINK), ofn.lpstrFile);
			break;
		case IDC_COMBOCHAR:
			GetWindowText(GetDlgItem(hDlg, IDC_COMBOCHAR), tempChar, 2);
			for (int i = 0; i < NUM_ALPHABET; i++)
			{
				if (wcscmp(tempChar, sthTable[i].myChar) == 0)
				{
					SetWindowText(GetDlgItem(hDlg, IDC_EDITLINK), sthTable[i].myLink);
				}
			}
			break;
		}
		break;
	case WM_SYSICON:
		if (lParam != WM_LBUTTONDOWN) break;
		ShowWindow(hDlg, SW_SHOW);
		Shell_NotifyIcon(NIM_DELETE, &notifyIconData);
		break;
	case WM_CLOSE:
		DestroyWindow(hDlg);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (INT_PTR)FALSE;
}
void InitNotifyIconData(NOTIFYICONDATA &notifyIconData,HWND hWnd)
{
	memset(&notifyIconData, 0, sizeof(NOTIFYICONDATA));
	notifyIconData.cbSize = sizeof(NOTIFYICONDATA);
	notifyIconData.hWnd = hWnd;
	notifyIconData.uID = ID_TRAY_APP_ICON;
	notifyIconData.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	notifyIconData.uCallbackMessage = WM_SYSICON; //Set up our invented Windows Message
	notifyIconData.hIcon = (HICON)LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_DRAWSOMETHING));
	wcscpy_s(notifyIconData.szTip, TEXT("DrawSomething"));
}